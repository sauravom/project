import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  file: any;
  data: any;
  constructor() { };

  ngOnInit() {
  }
  fileChanged(e) {
    this.file = e.target.files[0];
    console.log(this.file);
    
  }

  uploadDocument() {
    if (!this.file) {
      alert('Please select the files')
      return true
    };
    if(this.file.name){
      if(this.file.name.split('.')[1]!='csv'){
        alert('Please select the file in csv format');
        return true;
      }
    }
    let fileReader = new FileReader();
    fileReader.readAsText(this.file);
    fileReader.onload = (e) => {
      this.data = this.csvJSON(fileReader.result);
    }
  }

  //Changing Csv Data to Json
  csvJSON(csv) {
    let lines = csv.split("\n"),
      mainData = [];

    for (let j = 0; j < lines.length - 1; j++) {
      let contents = lines[j].split(","),
        newResult = {},
        result = [];
      for (let i = 1; i < contents.length; i++) {
        let key = contents[i].split("|");
        key = { year: contents[i].split("|")[0], score: contents[i].split("|")[1] };
        result.push(key);
      }
      newResult = { series: contents[0], array: this.sortedArray(result) };
      mainData.push(newResult)
    }
    return (mainData); //JSON
  }

  sortedArray(arr) {
    arr.sort((a, b) => {
      if (a.year < b.year) return -1;
      else if (a.year > b.year) return 1;
      else return 0;
    });
    return arr;
  }
}
